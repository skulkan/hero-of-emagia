<?php

namespace Hom;

use Hom\Observer\Observer;

class Battle
{
    /** @var Observer */
    private $observer;

    /**
     * Battle constructor.
     *
     * @param Observer $observer
     */
    public function __construct(Observer $observer)
    {
        $this->observer = $observer;
    }

    /**
     * Fight one on one
     *
     * @param Creature $creatureA
     * @param Creature $creatureB
     *
     * @return Creature|null
     */
    public function duel(Creature $creatureA, Creature $creatureB)
    {
        if ($creatureA->haveGreaterInitiativeThan($creatureB)) {
            $attacker = $creatureA;
            $defender = $creatureB;
        } else {
            $attacker = $creatureB;
            $defender = $creatureA;
        }

        $round = 0;
        while ($attacker->isAlive()) {
            $round++;

            $this->observer->lifeReport($round, $attacker, $defender);

            $dmg = $attacker->attack($defender);
            $this->observer->damageReport($round, $dmg, $attacker, $defender);

            if (!$defender->isAlive()) {
                break;
            }

            $dmg = $defender->attack($attacker);
            $this->observer->damageReport($round, $dmg, $defender, $attacker);

            if ($round >= 20) {
                $this->observer->noWinnerReport($round);

                return null;
            }
        }

        $winner = $defender;
        if ($attacker->isAlive()) {
            $winner = $attacker;
        }
        $this->observer->winnerReport($round, $winner);

        return $winner;
    }
}
