<?php

namespace Hom;

use Hom\Observer\Observer;
use Hom\Skills\Skill;

class Creature
{
    /** @var string */
    private $name;

    /** @var int */
    private $health;

    /** @var int */
    private $strength;

    /** @var int */
    private $defence;

    /** @var int */
    private $speed;

    /** @var int in percents 0 - 100 */
    private $luck;

    /** @var int */
    private $attacksPerRound = 1;

    /** @var int in percents 0 - 100 */
    private $takeLessDamage = 0;

    /** @var Skill[] */
    private $skills;

    /**
     * Creature constructor.
     *
     * @param string $name
     * @param int    $health
     * @param int    $strength
     * @param int    $defence
     * @param int    $speed
     * @param int    $luck
     * @param array  $skills
     */
    public function __construct($name, $health, $strength, $defence, $speed, $luck, array $skills = [])
    {
        $this->name     = $name;
        $this->health   = $health;
        $this->strength = $strength;
        $this->defence  = $defence;
        $this->speed    = $speed;
        $this->luck     = $luck;
        $this->skills   = $skills;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getHealth()
    {
        return $this->health;
    }

    /**
     * @return int
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * @return int
     */
    public function getLuck()
    {
        return $this->luck;
    }

    /**
     * @return array
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * @return int
     */
    public function getStrength()
    {
        return $this->strength;
    }

    /**
     * @return int
     */
    public function getDefence()
    {
        return $this->defence;
    }

    /**
     * @return int
     */
    public function getAttacksPerRound()
    {
        return $this->attacksPerRound;
    }

    /**
     * @return int
     */
    public function getTakeLessDamage()
    {
        return $this->takeLessDamage;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param int $health
     */
    public function setHealth($health)
    {
        $this->health = $health;
    }

    /**
     * @param int $strength
     */
    public function setStrength($strength)
    {
        $this->strength = $strength;
    }

    /**
     * @param int $defence
     */
    public function setDefence($defence)
    {
        $this->defence = $defence;
    }

    /**
     * @param int $speed
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;
    }

    /**
     * @param int $luck
     */
    public function setLuck($luck)
    {
        $this->luck = $luck;
    }

    /**
     * @param int $attacksPerRound
     */
    public function setAttacksPerRound($attacksPerRound)
    {
        $this->attacksPerRound = $attacksPerRound;
    }

    /**
     * @param int $takeLessDamage
     */
    public function setTakeLessDamage($takeLessDamage)
    {
        $this->takeLessDamage = $takeLessDamage;
    }

    /**
     * @param Skill[] $skills
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;
    }

    /**
     * @return bool
     */
    public function isAlive()
    {
        return $this->health > 0;
    }

    /**
     * @param Creature $enemy
     *
     * @return int
     */
    public function attack(Creature $enemy)
    {
        $buffedCreature = $this->skillsBuffer();

        $damage = 0;
        for ($i = 1; $i <= $buffedCreature->getAttacksPerRound(); $i++) {
            $damage += $enemy->defend($buffedCreature->getStrength());
        }

        return $damage;
    }

    /**
     * @param int $attackStrength
     *
     * @return int
     */
    public function defend($attackStrength)
    {
        $buffedCreature = $this->skillsBuffer();

        $damage = $attackStrength - $buffedCreature->getDefence();
        $damage = intval($damage * (100 - $buffedCreature->getTakeLessDamage()) / 100);


        if ($damage < 1) {
            return 0;
        }

        if (rand(1, 100) <= $this->getLuck()) {
            return 0;
        }

        $buffedCreature->setHealth($buffedCreature->getHealth() - $damage);
        $this->setHealth($buffedCreature->getHealth());

        return $damage;
    }

    /**
     * @return Creature
     */
    private function skillsBuffer()
    {
        $creature = clone $this;

        foreach ($this->skills as $skill) {
            if ($skill->shouldOccurs()) {
                $skill->buffCreature($creature);
            }
        }

        return $creature;
    }

    /**
     * @param Creature $enemy
     *
     * @return bool
     */
    public function haveGreaterInitiativeThan(Creature $enemy)
    {
        if ($this->getSpeed() > $enemy->getSpeed()) {
            return true;
        }

        if ($this->getSpeed() < $enemy->getSpeed()) {
            return false;
        }

        if ($this->getLuck() > $enemy->getLuck()) {
            return true;
        }

        if ($this->getLuck() < $enemy->getLuck()) {
            return false;
        }

        // There is nothing in task but if all is equal we will draw who will start
        if (rand(1, 100) > 50) {
            return true;
        }

        return false;
    }
}
