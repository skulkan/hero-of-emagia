<?php

namespace Hom;

use Hom\Skills\AdditionalAttack;
use Hom\Skills\MagicalShield;
use Hom\Skills\Vitality;

class CreatureSpawn
{
    public static function spawnHero()
    {
        $creature = new Creature(
            'Orderus',
            rand(70, 100),
            rand(70, 80),
            rand(45, 55),
            rand(40, 50),
            rand(10, 30)
        );

        $creature->setSkills([
            new AdditionalAttack(),
            new MagicalShield(),
        ]);

        return $creature;
    }

    public static function spawnWildBeast()
    {
        $creature = new Creature(
            'WildBeast',
            rand(60, 90),
            rand(60, 90),
            rand(40, 60),
            rand(40, 60),
            rand(25, 40)
        );

        $creature->setSkills([
            new Vitality()
        ]);

        return $creature;
    }
}