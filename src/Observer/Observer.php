<?php

namespace Hom\Observer;

use Hom\Creature;

interface Observer
{
    /**
     * @param int      $round
     * @param Creature $creatureA
     * @param Creature $creatureB
     */
    public function lifeReport($round, Creature $creatureA, Creature $creatureB);

    /**
     * @param int      $round
     * @param int      $damageAmount
     * @param Creature $from
     * @param Creature $to
     */
    public function damageReport($round, $damageAmount, Creature $from, Creature $to);

    /**
     * @param int      $round
     * @param Creature $winner
     */
    public function winnerReport($round, Creature $winner);

    /**
     * @param int $round
     */
    public function noWinnerReport($round);
}
