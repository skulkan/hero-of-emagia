<?php

namespace Hom\Observer;

use Hom\Creature;

/**
 * Class Buffer sends communicates to array buffer
 *
 * @author Sławomir Roszczyk <skulkan@gmail.com>
 */
class Buffer implements Observer
{
    /** @var array */
    private $buffer = [];

    /**
     * @inheritdoc
     */
    public function lifeReport($round, Creature $creatureA, Creature $creatureB)
    {
        $this->buffer[] = sprintf(
            "%d: %s HP = %d, %s HP = %d",
            $round,
            $creatureA->getName(),
            $creatureA->getHealth(),
            $creatureB->getName(),
            $creatureB->getHealth()
        );
    }

    /**
     * @inheritdoc
     */
    public function damageReport($round, $damageAmount, Creature $from, Creature $to)
    {
        $this->buffer[] = sprintf(
            "%d: %s deal %d damage to %s",
            $round,
            $from->getName(),
            $damageAmount,
            $to->getName()
        );
    }

    /**
     * @inheritdoc
     */
    public function winnerReport($round, Creature $winner)
    {
        $this->buffer[] = sprintf("%d: %s wins!!!", $round, $winner->getName());
    }

    /**
     * @inheritdoc
     */
    public function noWinnerReport($round)
    {
        $this->buffer[] = sprintf("%d: we have no winner!!!", $round);
    }

    /**
     * @return array
     */
    public function getBuffer()
    {
        return $this->buffer;
    }
}
