<?php

namespace Hom\Observer;

use Hom\Creature;

/**
 * Class OnScreen sends communicates at once to the screen
 *
 * @author Sławomir Roszczyk <skulkan@gmail.com>
 */
class OnScreen implements Observer
{
    /**
     * @inheritdoc
     */
    public function lifeReport($round, Creature $creatureA, Creature $creatureB)
    {
        printf(
            "%d: %s HP = %d, %s HP = %d\n",
            $round,
            $creatureA->getName(),
            $creatureA->getHealth(),
            $creatureB->getName(),
            $creatureB->getHealth()
        );
    }

    /**
     * @inheritdoc
     */
    public function damageReport($round, $damageAmount, Creature $from, Creature $to)
    {
        printf("%d: %s deal %d damage to %s\n", $round, $from->getName(), $damageAmount, $to->getName());
    }

    /**
     * @inheritdoc
     */
    public function winnerReport($round, Creature $winner)
    {
        printf("-----\n%d: %s wins!!!\n", $round, $winner->getName());
    }

    /**
     * @inheritdoc
     */
    public function noWinnerReport($round)
    {
        printf("------\n%d: we have no winner!!!\n", $round);
    }
}
