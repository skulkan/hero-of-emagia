<?php

namespace Hom\Skills;

use Hom\Creature;

class Vitality implements Skill
{
    /**
     * @inheritdoc
     */
    public function shouldOccurs()
    {
        return round(1, 100) <= 25;
    }

    /**
     * @inheritdoc
     */
    public function buffCreature(Creature $creature)
    {
        $creature->setHealth($creature->getHealth() + 15);

        return $creature;
    }
}