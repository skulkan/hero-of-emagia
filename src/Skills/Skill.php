<?php
/** Copyright 2017 ITHGroup. */

namespace Hom\Skills;

use Hom\Creature;

interface Skill
{
    /**
     * @return bool
     */
    public function shouldOccurs();

    /**
     * Update creature basic statistics
     *
     * @param Creature $creature
     *
     * @return Creature
     */
    public function buffCreature(Creature $creature);
}
