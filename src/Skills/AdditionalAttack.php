<?php

namespace Hom\Skills;

use Hom\Creature;

class AdditionalAttack implements Skill
{
    /**
     * @inheritdoc
     */
    public function shouldOccurs()
    {
        return round(1, 100) <= 10;
    }

    /**
     * @inheritdoc
     */
    public function buffCreature(Creature $creature)
    {
        $creature->setAttacksPerRound($creature->getAttacksPerRound() * 2);

        return $creature;
    }
}