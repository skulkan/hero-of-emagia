<?php

namespace Hom\Skills;

use Hom\Creature;

class MagicalShield implements Skill
{
    /**
     * @inheritdoc
     */
    public function shouldOccurs()
    {
        return round(1, 100) <= 20;
    }

    /**
     * @inheritdoc
     */
    public function buffCreature(Creature $creature)
    {
        $creature->setTakeLessDamage(50);

        return $creature;
    }
}