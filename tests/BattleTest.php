<?php

use Hom\Battle;
use Hom\Creature;
use Hom\Observer\Buffer;
use PHPUnit\Framework\TestCase;

class BattleTest extends TestCase
{
    /**
     * @dataProvider provideDuel
     */
    public function testDuel($creatureA, $creatureB, $winnerName)
    {
        $battle = new Battle(new Buffer());
        $winner = $battle->duel($creatureA, $creatureB);

        $this->assertEquals($winnerName, $winner->getName());
    }

    public function provideDuel()
    {
        return [
            [
                new Creature('A', 1, 0, 0, 0, 0),
                new Creature('B', 0, 0, 0, 0, 0),
                'A',
            ],
            [
                new Creature('A', 1, 1, 0, 0, 0),
                new Creature('B', 1, 1, 0, 1, 0),
                'B',
            ]
        ];
    }

    public function testNoWinner()
    {
        $battle = new Battle(new Buffer());
        $winner = $battle->duel(new Creature('A', 1, 1, 1, 0, 0), new Creature('A', 1, 1, 1, 0, 0));

        $this->assertNull($winner);
    }
}
