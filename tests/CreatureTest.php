<?php

use Hom\Creature;
use PHPUnit\Framework\TestCase;

class CreatureTest extends TestCase
{
    public function testDeathFromBegining()
    {
        $creature = new Creature('Death', 0, 0, 0, 0, 0);

        $this->assertFalse($creature->isAlive());
    }

    /**
     * @dataProvider provideDieInOneHit
     */
    public function testDieInOneHit($defenderHealth, $defenderDefence, $attackerStrength, $willDieInOneHit)
    {
        $creatureA = new Creature('A', $defenderHealth, 0, $defenderDefence, 0, 0);
        $creatureB = new Creature('B', 0, $attackerStrength, 0, 0, 0);

        $this->assertTrue($creatureA->isAlive());

        $creatureB->attack($creatureA);

        $this->assertEquals($willDieInOneHit, !$creatureA->isAlive());
    }

    public function provideDieInOneHit()
    {
        return [
            [1, 0, 1, true],
            [1, 0, 0, false],
            [1, 1, 1, false],
            [3, 1, 4, true],
            [4, 1, 4, false],
        ];
    }

    public function testWeakCreatureNeverKill()
    {
        $creatureA = new Creature('A', 1, 0, 5, 0, 0);
        $creatureB = new Creature('B', 0, 2, 0, 0, 0);

        $creatureB->attack($creatureA);
        $creatureB->attack($creatureA);
        $creatureB->attack($creatureA);
        $creatureB->attack($creatureA);
        $creatureB->attack($creatureA);
        $creatureB->attack($creatureA);
        $creatureB->attack($creatureA);
        $creatureB->attack($creatureA);

        $this->assertTrue($creatureA->isAlive());
    }

    public function testThatStrengthLowerThanDefenceDoesNotResurect()
    {
        $creatureA = new Creature('A', 0, 0, 1, 0, 0);
        $creatureB = new Creature('B', 0, 0, 0, 0, 0);

        $this->assertFalse($creatureA->isAlive());

        $creatureB->attack($creatureA);

        $this->assertFalse($creatureA->isAlive());
    }

    /**
     * @dataProvider providerHaveGreaterInitiativeThan
     */
    public function testHaveGreaterInitiativeThan($speedA, $speedB, $luckA, $luckB, $greaterIsForA)
    {
        $creatureA = new Creature('A', 0, 0, 0, $speedA, $luckA);
        $creatureB = new Creature('B', 0, 0, 0, $speedB, $luckB);

        $this->assertEquals($greaterIsForA, $creatureA->haveGreaterInitiativeThan($creatureB));
    }

    public function providerHaveGreaterInitiativeThan()
    {
        return [
            [1, 0, 0, 0, true],
            [0, 1, 0, 0, false],
            [1, 0, 0, 1, true],
            [0, 1, 1, 0, false],
            [1, 1, 1, 0, true],
            [1, 1, 0, 1, false],
        ];
    }

    public function testEqualInitiative()
    {
        $creatureA = new Creature('A', 0, 0, 0, 0, 0);
        $creatureB = new Creature('B', 0, 0, 0, 0, 0);

        $this->assertTrue(is_bool($creatureA->haveGreaterInitiativeThan($creatureB)));
    }

    public function testLuckyGuy()
    {
        $creatureA = new Creature('LuckyLuck', 1, 0, 0, 0, 100);
        $creatureB = new Creature('DickDigger', 1, 100, 0, 0, 0);

        $creatureB->attack($creatureA);
        $creatureB->attack($creatureA);
        $creatureB->attack($creatureA);
        $creatureB->attack($creatureA);
        $creatureB->attack($creatureA);
        $creatureB->attack($creatureA);
        $creatureB->attack($creatureA);

        $this->assertTrue($creatureA->isAlive());
    }
}
