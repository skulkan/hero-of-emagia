<?php

use Hom\Creature;
use Hom\Skills\AdditionalAttack;
use Hom\Skills\MagicalShield;
use Hom\Skills\Vitality;
use PHPUnit\Framework\TestCase;

class CreatureSkillsTest extends TestCase
{
    public function testAdditionalAttackShouldKillInOneAttack()
    {
        $skillMock = $this->getMockBuilder(AdditionalAttack::class)
            ->setMethods(['shouldOccurs'])
            ->getMock();
        $skillMock->method('shouldOccurs')->will($this->returnValue(true));

        $creatureA = new Creature('A', 1, 2, 0, 0, 0, [$skillMock]);
        $creatureB = new Creature('B', 3, 0, 0, 0, 0);

        $this->assertTrue($creatureB->isAlive());

        $damage = $creatureA->attack($creatureB);

        $this->assertEquals(4, $damage);
        $this->assertFalse($creatureB->isAlive());
    }

    public function testMagicalShieldShouldAllowToSurviveOneBigHit()
    {
        $skillMock = $this->getMockBuilder(MagicalShield::class)
                          ->setMethods(['shouldOccurs'])
                          ->getMock();
        $skillMock->method('shouldOccurs')->will($this->returnValue(true));

        $creatureA = new Creature('A', 1, 4, 0, 0, 0);
        $creatureB = new Creature('B', 3, 0, 0, 0, 0, [$skillMock]);

        $damage = $creatureA->attack($creatureB);

        $this->assertEquals(2, $damage);
        $this->assertTrue($creatureB->isAlive());
    }

    public function testVitalityShouldNotAllowToDeath()
    {
        $skillMock = $this->getMockBuilder(Vitality::class)
                          ->setMethods(['shouldOccurs'])
                          ->getMock();
        $skillMock->method('shouldOccurs')->will($this->returnValue(true));

        $creatureA = new Creature('A', 1, 15, 0, 0, 0);
        $creatureB = new Creature('B', 3, 0, 0, 0, 0, [$skillMock]);

        $damage = $creatureA->attack($creatureB);

        $this->assertEquals(15, $damage);
        $this->assertTrue($creatureB->isAlive());

        $damage = $creatureA->attack($creatureB);

        $this->assertEquals(15, $damage);
        $this->assertTrue($creatureB->isAlive());

        $damage = $creatureA->attack($creatureB);

        $this->assertEquals(15, $damage);
        $this->assertTrue($creatureB->isAlive());
    }

    public function testThatSkillsShouldOccursReturnBool()
    {
        $this->assertInternalType('bool', (new AdditionalAttack())->shouldOccurs());
        $this->assertInternalType('bool', (new MagicalShield())->shouldOccurs());
        $this->assertInternalType('bool', (new Vitality())->shouldOccurs());
    }
}
