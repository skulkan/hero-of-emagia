<?php

require 'vendor/autoload.php';

$battle = new \Hom\Battle(new \Hom\Observer\OnScreen());

$battle->duel(
    \Hom\CreatureSpawn::spawnHero(),
    \Hom\CreatureSpawn::spawnWildBeast()
);
